# 3. spa mobile web framework

Date: 2021-04-20

## Status

Proposed

## Context

K는 React, Vue중 learning curve가 완만하여 유지보수가 용이할 것이라는 이유로 Vue 선호

Concerning points

- 인력 소싱 : Vue 경력 인력이 없다
- 협력사에서 sourcing된 개발자의 의견을 물었을 때 CNS에서 leading을 하면 Vue 개발을 배우며 할 수 있다는 답변
- 개발 기간 : 기존 화면(200본)을 대부분 재개발해야 하는데, 기간내 가능할 것인가?

Vue 2 or 3

- Vue.js 3.0이 release되었지만 아직 대부분의 component들이 아직 Vue2까지만 지원하고 있다
- 기존 SPA 솔루션 사용 방안 : JSP와 jQuery 기반 코드로 Techinical Debt를 안고 가야 함
- Vue2로 기본 골격을 구성했지만, 투입 시기가 늦어져 Vue2를 쓰기에도 연말이면 Technical Debt로 유지보수가 어려울 수 있다
- Vue2 + Typescript 조합에서도 Composition 구성으로, Vue3와 사용법에 별반 차이가 없다

## Decision

Vue.js 3.0을 SPA framework으로 택하여 개발한다

## Consequences

Benefits

- 고객 요청 사항 만족
- 충분한 개발 참고 사례 구글 검색 가능
- 향후 유지 보수 용이

Risks and Migigation

- Vue 경력 없는 개발 인력

 -> 큰 구조를 본사에서 잡고, 초반 집중 교육과 Code Review를 통해 개발 역량 키운다

- Vue publishing이 없는 디자이너

 -> Figma로 component 디자인, publishing하고 storybook을 통해 개발자와 협업한다
